using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MiniCarsales.Models.Validators;
using MiniCarsales.Models.Common;
using MiniCarsales.Services;

namespace MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    public class EngineController : Controller
    {
        private readonly ICommonPropertyServices _commPropertyServices;
        private readonly ILogger _logger;

        public EngineController(ICommonPropertyServices commPropertyServices, ILogger<EngineController> logger)
        {
            _commPropertyServices = commPropertyServices;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Engine>> Get()
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get engines received ...");
            IEnumerable<Engine> engines = _commPropertyServices.GetEngines();

            return Ok(engines);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Engine> Post([FromBody] Engine engine)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Post creating engines received ...");
            var createdEngine = _commPropertyServices.AddEngine(engine);
            if (createdEngine == null)
            {
                _logger.LogError("Internal errors, creating engines failed");
                return StatusCode(StatusCodes.Status500InternalServerError, "Creating engine failed");
            }

            return new CreatedResult(nameof(Post), createdEngine);
        }
    }
}
