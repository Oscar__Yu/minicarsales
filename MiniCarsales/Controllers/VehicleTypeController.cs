using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MiniCarsales.Models.Validators;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Services;

namespace MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    public class VehicleTypeController : Controller
    {
        private readonly IVehicleTypeServices _vehicleTypeServices;
        private readonly ILogger _logger;

        public VehicleTypeController(IVehicleTypeServices vehicleTypeServices, ILogger<VehicleTypeController> logger)
        {
            _vehicleTypeServices = vehicleTypeServices;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<VehicleType>> Get()
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get vehicle types received ...");
            IEnumerable<VehicleType> vehicleTypes = _vehicleTypeServices.GetVehicleTypes();

            return Ok(vehicleTypes);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Car> Post([FromBody] VehicleType vehicleType)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Post creating vehicle types received ...");
            var createdVehicleType = _vehicleTypeServices.AddVehicleType(vehicleType);
            if (createdVehicleType == null)
            {
                _logger.LogError("Internal errors, creating vehicle type failed");
                return StatusCode(StatusCodes.Status500InternalServerError, "Creating vehicle type failed");
            }

            return new CreatedResult(nameof(Post), createdVehicleType);
        }
    }
}
