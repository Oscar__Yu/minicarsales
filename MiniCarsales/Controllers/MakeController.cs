using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MiniCarsales.Models.Validators;
using MiniCarsales.Models.Common;
using MiniCarsales.Services;

namespace MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    public class MakeController : Controller
    {
        private readonly ICommonPropertyServices _commPropertyServices;
        private readonly ILogger _logger;

        public MakeController(ICommonPropertyServices commPropertyServices, ILogger<MakeController> logger)
        {
            _commPropertyServices = commPropertyServices;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Make>> Get()
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get makes received ...");
            IEnumerable<Make> makes = _commPropertyServices.GetMakes();

            return Ok(makes);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Make> Post([FromBody] Make make)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Post creating makes received ...");
            var createdMake = _commPropertyServices.AddMake(make);
            if (createdMake == null)
            {
                _logger.LogError("Internal errors, creating make failed");
                return StatusCode(StatusCodes.Status500InternalServerError, "Creating make failed");
            }

            return new CreatedResult(nameof(Post), createdMake);
        }
    }
}
