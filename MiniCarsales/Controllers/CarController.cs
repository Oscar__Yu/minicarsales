using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Models.Validators;
using MiniCarsales.Services;
using MiniCarsales.Models.Contracts;

namespace MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    public class CarController : Controller
    {
        private readonly ICarServices _carServices;
        private readonly ILogger _logger;

        public CarController(ICarServices carServices, ILogger<CarController> logger)
        {
            _carServices = carServices;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<CarContractModel>> Get()
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get cars received ...");
            IEnumerable<CarContractModel> cars = _carServices.GetCars();

            return Ok(cars);
        }

        [HttpGet("bodytype")]
        public ActionResult<IEnumerable<BodyType>> GetBodyType()
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get body type received ...");
            IEnumerable<BodyType> bodyTypes = _carServices.GetBodyTypes();

            return Ok(bodyTypes);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<CarContractModel> Post([FromBody] CarContractModel car)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Post create car received ...");
            var createdCar = _carServices.AddCar(car);
            if (createdCar == null)
            {
                _logger.LogError("Internal errors, creating car failed");
                return StatusCode(StatusCodes.Status500InternalServerError, "Creating car failed");
            }

            return new CreatedResult(nameof(Post), car);
        }
    }
}
