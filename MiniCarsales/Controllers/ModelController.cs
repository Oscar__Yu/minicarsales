using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MiniCarsales.Models.Validators;
using MiniCarsales.Models.Common;
using MiniCarsales.Services;

namespace MiniCarsales.Controllers
{
    [Route("api/[controller]")]
    public class ModelController : Controller
    {
        private readonly ICommonPropertyServices _commPropertyServices;
        private readonly ILogger _logger;

        public ModelController(ICommonPropertyServices commPropertyServices, ILogger<ModelController> logger)
        {
            _commPropertyServices = commPropertyServices;
            _logger = logger;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Model>> Get(int makeId)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Get models received ...");
            IEnumerable<Model> models = _commPropertyServices.GetModelsByMake(makeId);

            return Ok(models);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public ActionResult<Model> Post([FromBody] Model model)
        {
            if (!ModelState.IsValid)
            {
                var errMessage = ErrorUtil.GetErrorMessage(ModelState);
                _logger.LogError(errMessage);
                return BadRequest(errMessage);
            }

            _logger.LogInformation("Post creating model received ...");
            var createdModel = _commPropertyServices.AddModel(model);
            if (createdModel == null)
            {
                _logger.LogError("Internal errors, creating model failed");
                return StatusCode(StatusCodes.Status500InternalServerError, "Creating model failed");
            }

            return new CreatedResult(nameof(Post), createdModel);
        }
    }
}
