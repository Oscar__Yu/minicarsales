﻿using System.Collections.Generic;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.DataAccessors;
using MiniCarsales.Models.Contracts;

namespace MiniCarsales.Services
{
    public interface ICarServices
    {
        CarContractModel AddCar(CarContractModel car);
        BodyType AddBodyType(BodyType bodyType);
        IEnumerable<CarContractModel> GetCars();
        IEnumerable<BodyType> GetBodyTypes();
    }

    public class CarServices : ICarServices
    {
        private readonly ICarDataAccessor _carDataAccessor;
        private readonly IBodyTypeDataAccessor _bodyTypeDataAccessor;

        public CarServices(
            ICarDataAccessor carDataAccessor,
            IBodyTypeDataAccessor bodyTypeDataAccessor
            )
        {
            _carDataAccessor = carDataAccessor;
            _bodyTypeDataAccessor = bodyTypeDataAccessor;
        }

        public CarContractModel AddCar(CarContractModel car)
        {
            var dbInstance = new Car()
            {
                Id = car.Id,
                VehicleTypeId = car.VehicleTypeId,
                Doors = car.Doors,
                MakeId = car.MakeId,
                EngineId = car.EngineId,
                BodyTypeId = car.BodyTypeId,
                ModelId = car.ModelId
            };
            var carCreated = _carDataAccessor.Add(dbInstance);

            return new CarContractModel()
            {
                Id = carCreated.Id,
                VehicleTypeId = carCreated.VehicleTypeId,
                Doors = carCreated.Doors,
                MakeId = carCreated.MakeId,
                MakeName = carCreated.Make?.Name,
                ModelId = carCreated.ModelId,
                ModelName = carCreated.Model?.Name,
                EngineId = carCreated.EngineId,
                EngineName = carCreated.Engine?.Name,
                BodyTypeId = carCreated.BodyTypeId,
                BodyTypeName = carCreated.BodyType?.Name
            };
        }

        public BodyType AddBodyType(BodyType bodyType)
        {
            var bodyTypeCreated = _bodyTypeDataAccessor.Add(bodyType);

            return bodyTypeCreated;
        }

        public IEnumerable<CarContractModel> GetCars()
        {
            return _carDataAccessor.GetAll();
        }

        public IEnumerable<BodyType> GetBodyTypes()
        {
            return _bodyTypeDataAccessor.GetAll();
        }
    }
}
