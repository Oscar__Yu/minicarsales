﻿using System.Collections.Generic;
using MiniCarsales.DataAccessors;
using MiniCarsales.Models.Common;

namespace MiniCarsales.Services
{
    public interface ICommonPropertyServices
    {
        IEnumerable<Engine> GetEngines();
        IEnumerable<Make> GetMakes();
        IEnumerable<Model> GetModelsByMake(int makeId);
        Make AddMake(Make make);
        Engine AddEngine(Engine engine);
        Model AddModel(Model model);
    }

    public class CommonPropertyServices : ICommonPropertyServices
    {
        private readonly IMakeDataAccessor _makeDataAccessor;
        private readonly IModelDataAccessor _modelDataAccessor;
        private readonly IEngineDataAccessor _engineDataAccessor;

        public CommonPropertyServices(
            IMakeDataAccessor makeDataAccessor,
            IModelDataAccessor modelDataAccessor,
            IEngineDataAccessor engineDataAccessor
            )
        {
            _engineDataAccessor = engineDataAccessor;
            _makeDataAccessor = makeDataAccessor;
            _modelDataAccessor = modelDataAccessor;
        }

        public Make AddMake(Make make)
        {
            var makeCreated = _makeDataAccessor.Add(make);

            return makeCreated;
        }

        public Model AddModel(Model model)
        {
            var modelCreated = _modelDataAccessor.Add(model);

            return modelCreated;
        }

        public Engine AddEngine(Engine engine)
        {
            var engineCreated = _engineDataAccessor.Add(engine);

            return engineCreated;
        }

        public IEnumerable<Model> GetModelsByMake(int makeId)
        {
            return _modelDataAccessor.GetByMake(makeId);
        }

        public IEnumerable<Engine> GetEngines()
        {
            return _engineDataAccessor.GetAll();
        }

        public IEnumerable<Make> GetMakes()
        {
            return _makeDataAccessor.GetAll();
        }

    }
}
