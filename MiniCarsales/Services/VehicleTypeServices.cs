﻿using System.Collections.Generic;
using MiniCarsales.DataAccessors;
using MiniCarsales.Models.Vehicle;

namespace MiniCarsales.Services
{
    public interface IVehicleTypeServices
    {
        VehicleType AddVehicleType(VehicleType vehicleType);
		IEnumerable<VehicleType> GetVehicleTypes();
	}

    public class VehicleTypeServices : IVehicleTypeServices
    {
        private readonly IVehicleTypeDataAccessor _vehicleTypeDataAccessor;

        public VehicleTypeServices(IVehicleTypeDataAccessor vehicleTypeDataAccessor)
        {
            _vehicleTypeDataAccessor = vehicleTypeDataAccessor;
        }

        public VehicleType AddVehicleType(VehicleType vehicleType)
        {
            var vehicleTypeCreated = _vehicleTypeDataAccessor.Add(vehicleType);

            return vehicleTypeCreated;
        }

		public IEnumerable<VehicleType> GetVehicleTypes()
        {
            return _vehicleTypeDataAccessor.GetAll();
        }
	}
}
