using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using MiniCarsales.Database;

namespace MiniCarsales
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateWebHostBuilder(args).Build();
            SeedData(host);
            host.Run();
        }

        public static void SeedData(IWebHost host)
        {
            using (var scope = host.Services.CreateScope())
            {
                // Seed data into InMemory DB
                var services = scope.ServiceProvider;
                var context = services.GetRequiredService<DataStore>();
                DataStoreSeed.Seed(context);
            }
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
