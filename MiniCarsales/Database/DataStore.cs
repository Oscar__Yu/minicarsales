﻿using Microsoft.EntityFrameworkCore;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Models.Common;

namespace MiniCarsales.Database
{
    public class DataStore : DbContext
    {
        public DataStore(DbContextOptions<DataStore> options)
            : base(options)
        {
        }

        public DbSet<Car> Cars { get; set; }

        public DbSet<BodyType> BodyTypes { get; set; }

        public DbSet<VehicleType> VehicleTypes { get; set; }

        public DbSet<Make> Makes { get; set; }

        public DbSet<Model> Models { get; set; }

        public DbSet<Engine> Engines { get; set; }
    }
}
