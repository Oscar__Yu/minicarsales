﻿using System.Collections.Generic;
using MiniCarsales.DataAccessors;
using MiniCarsales.Models.Common;
using MiniCarsales.Models.Vehicle;

namespace MiniCarsales.Database
{
    public static class DataStoreSeed
    {
        public static void Seed(DataStore dataStore)
        {
            // Seed car body types
            var availableBodyType = new List<BodyType>{
                new BodyType { Name="Hatchback" },
                new BodyType { Name="Sedan" }
            };
            var bodyTypeDataAccessor = new BodyTypeDataAccessor(dataStore);
            foreach (var type in availableBodyType)
            {
                bodyTypeDataAccessor.Add(type);
            }

            // Seed vehicle types
            var availableVehicleType = new List<VehicleType>
            {
                new VehicleType { Name="Car" }
            };
            var vehicleTypeDataAccessor = new VehicleTypeDataAccessor(dataStore);
            foreach (var type in availableVehicleType)
            {
                vehicleTypeDataAccessor.Add(type);
            }

            // Seed Makes
            var availableMakes = new List<Make>
            {
                new Make { Name="Toyota" },
                new Make { Name="Ford" }
            };
            var makeDataAccessor = new MakeDataAccessor(dataStore);
            foreach (var make in availableMakes)
            {
                makeDataAccessor.Add(make);
            }

            // Seed Models
            var availableModels = new List<Model>
            {
                new Model { Name="Camry", MakeId=1 },
                new Model { Name="EcoSport", MakeId=2 }
            };
            var modelDataAccessor = new ModelDataAccessor(dataStore);
            foreach (var model in availableModels)
            {
                modelDataAccessor.Add(model);
            }

            // Seed Engines
            var availableEngines = new List<Engine>
            {
                new Engine { Name="Piston" },
                new Engine { Name="Electric" }
            };
            var engineDataAccessor = new EngineDataAccessor(dataStore);
            foreach (var engine in availableEngines)
            {
                engineDataAccessor.Add(engine);
            }

            dataStore.SaveChanges();
        }
    }
}
