using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MiniCarsales.DataAccessors;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;
using MiniCarsales.Models.Contracts;
using MiniCarsales.Models.Validators;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Services;

namespace MiniCarsales
{
    public class Startup
    {
        private readonly ILogger _logger;

        public Startup(IConfiguration configuration, ILogger<Startup> logger)
        {
            Configuration = configuration;
            _logger = logger;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Data accessors
            services.AddScoped<IMakeDataAccessor, MakeDataAccessor>();
            services.AddScoped<IEngineDataAccessor, EngineDataAccessor>();
            services.AddScoped<IModelDataAccessor, ModelDataAccessor>();
            services.AddScoped<ICarDataAccessor, CarDataAccessor>();
            services.AddScoped<IBodyTypeDataAccessor, BodyTypeDataAccessor>();
            services.AddScoped<IVehicleTypeDataAccessor, VehicleTypeDataAccessor>();

            _logger.LogDebug("Data accessors injected");

            // Services handling business logic
            services.AddScoped<ICarServices, CarServices>();
			services.AddScoped<ICommonPropertyServices, CommonPropertyServices>();
			services.AddScoped<IVehicleTypeServices, VehicleTypeServices>();

            _logger.LogDebug("Services injected");

            // Validation
            services.AddTransient<IValidator<Model>, ModelValidator>();
            services.AddTransient<IValidator<Make>, MakeValidator>();
            services.AddTransient<IValidator<Engine>, EngineValidator>();
            services.AddTransient<IValidator<CarContractModel>, CarValidator>();
            services.AddTransient<IValidator<VehicleType>, VehicleValidator>();

            _logger.LogDebug("Validators injected");

            services.AddDbContext<DataStore>(opt => opt.UseInMemoryDatabase("InMemoryDataStore"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddFluentValidation();

            _logger.LogDebug("DB and MVC with fluent validator injected");

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                _logger.LogDebug("Launched in dev env");

                app.UseDeveloperExceptionPage();
            }
            else
            {
                _logger.LogDebug("Launched in non-dev env");

                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });

            _logger.LogDebug("App setup completed ...");
        }
    }
}
