using FluentValidation;
using MiniCarsales.Models.Common;

namespace MiniCarsales.Models.Validators
{
    public class ModelValidator : AbstractValidator<Model>
    {
        public ModelValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Invalid model name.");
        }
    }

    public class MakeValidator : AbstractValidator<Make>
    {
        public MakeValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Invalid make name.");
        }
    }

    public class EngineValidator : AbstractValidator<Engine>
    {
        public EngineValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Invalid engine name.");
        }
    }
}
