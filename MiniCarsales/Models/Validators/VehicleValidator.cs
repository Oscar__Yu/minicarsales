using FluentValidation;
using MiniCarsales.Models.Vehicle;

namespace MiniCarsales.Models.Validators
{
    public class VehicleValidator : AbstractValidator<VehicleType>
    {
        public VehicleValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Invalid vehicle type name.");
        }
    }
}
