using FluentValidation;
using MiniCarsales.Models.Contracts;
using MiniCarsales.Models.Vehicle;

namespace MiniCarsales.Models.Validators
{
    public class CarValidator : AbstractValidator<CarContractModel>
    {
        public CarValidator()
        {
            RuleFor(x => x.VehicleTypeId).NotEmpty().WithMessage("Invalid vehicle type.");
            RuleFor(x => x.BodyTypeId).NotEmpty().WithMessage("Please choose the car's body type.");
            RuleFor(x => x.MakeId).NotEmpty().WithMessage("Please choose the car's make.");
            RuleFor(x => x.ModelId).NotEmpty().WithMessage("Please choose the car's model.");
            RuleFor(x => x.Doors).NotEmpty().GreaterThanOrEqualTo(2).WithMessage("Number of doors must be at least 2");
            RuleFor(x => x.EngineId).NotEmpty().WithMessage("Please choose the car's engine.");
        }
    }

    public class BodyTypeValidator : AbstractValidator<BodyType>
    {
        public BodyTypeValidator()
        {
            RuleFor(x => x.Name).NotEmpty().WithMessage("Invalid body type name.");
        }
    }
}
