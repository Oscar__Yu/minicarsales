using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace MiniCarsales.Models.Validators
{
    public static class ErrorUtil
    {
        public static string GetErrorMessage(ModelStateDictionary modelState)
        {
            var errorMessage = "";
            foreach (var state in modelState.Values)
            {
                foreach (var error in state.Errors)
                {
                    errorMessage += error.ErrorMessage + " ";
                }
            }

            return errorMessage;
        }
    }
}
