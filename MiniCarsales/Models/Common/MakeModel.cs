using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MiniCarsales.Models.Common
{
    [Table("Makes")]
    public class Make
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
