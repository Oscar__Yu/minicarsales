using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MiniCarsales.Models.Common
{
    [Table("Engines")]
    public class Engine
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
