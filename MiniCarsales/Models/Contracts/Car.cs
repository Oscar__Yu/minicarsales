﻿using MiniCarsales.Models.Vehicle;

namespace MiniCarsales.Models.Contracts
{
    /// <summary>
    /// Data type defined for client application, to avoid client code changes
    /// when changing Database table definition
    /// </summary>
    public class CarContractModel : IVehicle
    {
        public int Id { get; set; }
        public int VehicleTypeId { get; set; }
        public int Doors { get; set; }

        public int MakeId { get; set; }
        public string MakeName { get; set; }

        public int EngineId { get; set; }
        public string EngineName { get; set; }

        public int BodyTypeId { get; set; }
        public string BodyTypeName { get; set; }

        public int ModelId { get; set; }
        public string ModelName { get; set; }
    }

}
