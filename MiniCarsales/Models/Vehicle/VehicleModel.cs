﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MiniCarsales.Models.Vehicle
{
    public interface IVehicle
    {
        int Id { get; set; }
        int VehicleTypeId { get; set; }
        int MakeId { get; set; }
        int ModelId { get; set; }
    }

    [Table("VehicleTypes")]
    public class VehicleType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
