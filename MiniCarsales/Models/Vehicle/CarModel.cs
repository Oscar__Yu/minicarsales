using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MiniCarsales.Models.Common;

namespace MiniCarsales.Models.Vehicle
{
    [Table("Cars")]
    public class Car : IVehicle
    {
        [Key]
        public int Id { get; set; }

        public int Doors { get; set; }
        /// <summary>
        /// A car always has 4 wheels.
        /// </summary>
        public int Wheels => 4;

        public int VehicleTypeId { get; set; }

        public int MakeId { get; set; }
        [ForeignKey("MakeId")]
        public Make Make { get; set; }

        public int EngineId { get; set; }
        [ForeignKey("EngineId")]
        public Engine Engine { get; set; }

        public int BodyTypeId { get; set; }
        [ForeignKey("BodyTypeId")]
        public BodyType BodyType { get; set; }

        public int ModelId { get; set; }
        [ForeignKey("ModelId")]
        public Model Model { get; set; }
    }

    [Table("BodyTypes")]
    public class BodyType
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
