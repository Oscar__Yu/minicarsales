import styled from "styled-components";
import { DropdownToggle, DropdownMenu } from "reactstrap";

export const StyledDropdownToggle = styled(DropdownToggle)`
  width: 200px;

  && {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }
`;

export const StyledDropdownMenu = styled(DropdownMenu)`
  width: 200px;
`;
