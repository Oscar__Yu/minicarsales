import React from "react";
import styled from "styled-components";
import { Container } from "reactstrap";
import Header from "./Header";

const StyledLayoutDiv = styled.div({
  fontSize: 14
});

const Layout = props => (
  <StyledLayoutDiv>
    <Header />
    <Container>{props.children}</Container>
  </StyledLayoutDiv>
);

export default Layout;
