import React from "react";
import { Route } from "react-router";
import Layout from "./Layout";
import Home from "./Home";
import CreateCar from "./CreateVehicle/CreateCar/Index";

const RootView = () => (
  <Layout>
    <Route exact path="/" component={Home} />
    <Route path="/create-car" component={CreateCar} />
  </Layout>
);

export default RootView;
