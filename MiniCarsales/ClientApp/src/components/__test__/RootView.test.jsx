import React from "react";
import TestRenderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import RootView from "../RootView";

test("Root view must be rendered correctly", () => {
  const root = TestRenderer.create(
    <BrowserRouter>
      <RootView />
    </BrowserRouter>
  );
  const tree = root.toJSON();
  expect(tree).toMatchSnapshot();
});
