import React from "react";
import TestRenderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import Home from "../Home";

test("Home page must be rendered correctly", () => {
  const home = TestRenderer.create(
    <BrowserRouter>
      <Home />
    </BrowserRouter>
  );
  const tree = home.toJSON();
  expect(tree).toMatchSnapshot();
});
