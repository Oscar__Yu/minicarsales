import React from "react";
import TestRenderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import Header from "../Header";

test("Header must be rendered correctly", () => {
  const header = TestRenderer.create(
    <BrowserRouter>
      <Header />
    </BrowserRouter>
  );
  const tree = header.toJSON();
  expect(tree).toMatchSnapshot();
});
