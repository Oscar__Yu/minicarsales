import React from "react";
import TestRenderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import Layout from "../Layout";

test("Layout container must be rendered correctly", () => {
  const layout = TestRenderer.create(
    <BrowserRouter>
      <Layout />
    </BrowserRouter>
  );
  const tree = layout.toJSON();
  expect(tree).toMatchSnapshot();
});
