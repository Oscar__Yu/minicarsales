import React from "react";
import TestRenderer from "react-test-renderer";
import { BrowserRouter } from "react-router-dom";
import CreateVehicleDropdown from "../CreateVehicleDropdown";

test("Create Vehicle dropdown must be rendered correctly", () => {
  const dropdown = TestRenderer.create(
    <BrowserRouter>
      <CreateVehicleDropdown />
    </BrowserRouter>
  );
  const tree = dropdown.toJSON();
  expect(tree).toMatchSnapshot();
});
