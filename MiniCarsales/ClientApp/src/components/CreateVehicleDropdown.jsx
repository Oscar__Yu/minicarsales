﻿import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Dropdown, DropdownItem, NavLink } from "reactstrap";
import { StyledDropdownToggle, StyledDropdownMenu } from "./StyledDropdown";

const CreateVehicleDropdown = () => {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <Dropdown isOpen={isOpen} toggle={toggle}>
      <StyledDropdownToggle caret>create vehicle</StyledDropdownToggle>
      <StyledDropdownMenu>
        <DropdownItem>
          <NavLink tag={Link} className="text-dark" to="/create-car">
            create car
          </NavLink>
        </DropdownItem>
      </StyledDropdownMenu>
    </Dropdown>
  );
};

export default CreateVehicleDropdown;
