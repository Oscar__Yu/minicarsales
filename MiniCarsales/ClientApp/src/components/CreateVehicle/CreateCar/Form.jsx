﻿import React, { useState } from "react";
import styled from "styled-components";
import { Col, Button, Form, FormGroup, Label, Input } from "reactstrap";
import CarPropDropdown from "./CarPropsDropdown";

const NumberInputGroup = props => (
  <StyledFormGroup row>
    <Label for={props.id} sm={2}>
      {props.label}
    </Label>
    <Col sm={10}>
      <Input
        type="number"
        name={props.name}
        id={props.id}
        defaultValue={props.defaultValue}
        min={props.min}
        max={props.max}
        placeholder={props.label}
        onChange={props.onChange}
      />
    </Col>
  </StyledFormGroup>
);

const StyledFormGroup = styled(FormGroup)`
  display: flex;
  align-items: center;
`;

const DropdownInputGroup = props => (
  <StyledFormGroup row>
    <Label for={props.id} sm={2}>
      {props.label}
    </Label>
    <Col sm={4}>
      <CarPropDropdown
        optionMap={props.optionMap}
        onSelected={props.onChange}
        label={props.label}
        name={props.name}
        selectedLabel={props.selectedLabel}
      />
    </Col>
    <Col sm={6}>{props.hint}</Col>
  </StyledFormGroup>
);

const CreateCarForm = props => {
  const [state, setState] = useState({
    VehicleTypeId: 1,
    ModelId: undefined,
    MakeId: undefined,
    EngineId: undefined,
    BodyTypeId: undefined,
    Doors: 2
  });

  const handleChange = e => {
    setState({
      ...state,
      [e.target.name]: e.target.value
    });
  };

  const onSubmit = () => {
    fetch(`api/car`, {
      method: "POST",
      mode: "cors",
      cache: "no-cache",
      headers: {
        "Content-Type": "application/json"
      },
      redirect: "follow",
      body: JSON.stringify(state)
    })
      .then(resp => {
        if (resp.status !== 201) {
          return resp.text();
        } else {
          return resp.json();
        }
      })
      .then(respBody => {
        if (typeof respBody === "string") {
          props.onCreateError(respBody);
        } else {
          props.onCreated(respBody);
        }
      })
      .catch(e => {
        console.error(e);
      });
  };

  const getModels = makeId =>
    fetch(`api/model?makeId=${makeId}`)
      .then(resp => {
        if (resp.status !== 200) {
          return resp.text();
        } else {
          return resp.json();
        }
      })
      .then(respBody => {
        if (typeof respBody === "string") {
          props.onCreateError(respBody);
        } else {
          props.onModelLoaded(respBody);
        }
      });

  const onMakeChanged = e => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
      ModelId: undefined
    });
  };

  return (
    <>
      <Form>
        <DropdownInputGroup
          label={"Make"}
          name={"MakeId"}
          hint={state.MakeId === undefined ? "Please select make first." : ""}
          selectedLabel={props.makes[state.MakeId]}
          optionMap={props.makes}
          onChange={e => {
            onMakeChanged(e);
            getModels(e.target.value);
          }}
        />
        <DropdownInputGroup
          label={"Model"}
          name={"ModelId"}
          selectedLabel={props.models[state.ModelId]}
          optionMap={props.models}
          onChange={handleChange}
        />
        <DropdownInputGroup
          label={"Engine"}
          name={"EngineId"}
          selectedLabel={props.engines[state.EngineId]}
          optionMap={props.engines}
          onChange={handleChange}
        />
        <DropdownInputGroup
          label={"Body Type"}
          name={"BodyTypeId"}
          selectedLabel={props.bodyTypes[state.BodyTypeId]}
          optionMap={props.bodyTypes}
          onChange={handleChange}
        />
        <NumberInputGroup
          name={"Doors"}
          label={"Doors"}
          id={"Doors"}
          defaultValue={2}
          min={2}
          max={6}
          onChange={handleChange}
        />
      </Form>

      <Col sm={{ size: 10, offset: 2 }}>
        <Button onClick={onSubmit}>Submit</Button>
      </Col>
    </>
  );
};

export default CreateCarForm;
