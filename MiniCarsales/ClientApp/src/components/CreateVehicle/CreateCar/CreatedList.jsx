﻿import React from "react";

const CreatedList = props => (
  <table className="table table-striped">
    <thead>
      <tr>
        <th>Make</th>
        <th>Model</th>
        <th>Doors</th>
        <th>Engine</th>
        <th>Body Type</th>
      </tr>
    </thead>
    <tbody>
      {props.cars.map((car, i) => (
        <tr key={i}>
          <td>{car.makeName}</td>
          <td>{car.modelName}</td>
          <td>{car.doors}</td>
          <td>{car.engineName}</td>
          <td>{car.bodyTypeName}</td>
        </tr>
      ))}
    </tbody>
  </table>
);

export default CreatedList;
