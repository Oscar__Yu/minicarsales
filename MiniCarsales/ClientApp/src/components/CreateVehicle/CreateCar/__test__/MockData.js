export const cars = [
  {
    makeId: 1,
    modelId: 1,
    engineId: 1,
    doors: 3,
    bodyTypeId: 1
  }
];

export const makes = [
  {
    id: 1,
    name: "Make 1"
  }
];

export const models = [
  {
    id: 1,
    name: "Model 1"
  }
];

export const engines = [
  {
    id: 1,
    name: "Engine 1"
  }
];

export const bodyTypes = {
  "1": "Body type 1",
  "2": "Body type 2"
};
