import React from "react";
import TestRenderer from "react-test-renderer";
import { bodyTypes } from "./MockData";
import CarPropDropdown from "../CarPropsDropdown";

test("Select body type dropdown must be rendered correctly", () => {
  const dropdown = TestRenderer.create(
    <CarPropDropdown optionMap={bodyTypes} />
  );
  const tree = dropdown.toJSON();
  expect(tree).toMatchSnapshot();
});
