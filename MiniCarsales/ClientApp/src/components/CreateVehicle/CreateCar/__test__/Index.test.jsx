import React from "react";
import TestRenderer from "react-test-renderer";
import { cars, bodyTypes, models, makes, engines } from "./MockData";
import CreateCar from "../Index";

describe("Create Car Page", () => {
  beforeAll(() => {
    global["fetch"] = jest.fn().mockImplementation(url =>
      Promise.resolve({
        ok: true,
        json: () => {
          switch (url) {
            case `api/car`:
              return Promise.resolve(cars);
            case `api/car/bodytype`:
              return Promise.resolve(
                Object(bodyTypes)
                  .keys()
                  .map(key => ({ name: bodyTypes[key], id: key }))
              );
            case `api/model`:
              return Promise.resolve(models);
            case `api/make`:
              return Promise.resolve(makes);
            case `api/engine`:
              return Promise.resolve(engines);
            default:
              return Promise.resolve();
          }
        }
      })
    );
  });

  afterAll(() => {
    global["fetch"].mockClear();
    delete global["fetch"];
  });

  test("Create car root view must be rendered correctly", () => {
    const dropdown = TestRenderer.create(<CreateCar />);
    const tree = dropdown.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
