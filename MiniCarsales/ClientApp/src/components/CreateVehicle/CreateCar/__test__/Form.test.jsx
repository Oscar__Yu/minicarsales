import React from "react";
import TestRenderer from "react-test-renderer";
import CreateCarForm from "../Form";
import { cars, bodyTypes, makes, models, engines } from "./MockData";

test("Create car form must be rendered correctly", () => {
  const dropdown = TestRenderer.create(
    <CreateCarForm
      bodyTypes={bodyTypes}
      cars={cars}
      makes={makes.reduce((map, make) => {
        map[make.id] = make.name;
        return map;
      }, {})}
      models={models.reduce((map, model) => {
        map[model.id] = model.name;
        return map;
      }, {})}
      engines={engines.reduce((map, engine) => {
        map[engine.id] = engine.name;
        return map;
      }, {})}
    />
  );
  const tree = dropdown.toJSON();
  expect(tree).toMatchSnapshot();
});
