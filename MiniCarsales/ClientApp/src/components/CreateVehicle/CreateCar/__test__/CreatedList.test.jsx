import React from "react";
import TestRenderer from "react-test-renderer";
import CreatedList from "../CreatedList";
import { cars } from "./MockData";

test("Created car list must be rendered correctly", () => {
  const dropdown = TestRenderer.create(<CreatedList cars={cars} />);
  const tree = dropdown.toJSON();
  expect(tree).toMatchSnapshot();
});
