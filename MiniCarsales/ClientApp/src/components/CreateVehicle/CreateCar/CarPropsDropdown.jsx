﻿import React, { useState } from "react";
import { Dropdown, DropdownItem } from "reactstrap";
import { StyledDropdownToggle, StyledDropdownMenu } from "../../StyledDropdown";

const CarPropDropdown = props => {
  const [isOpen, setIsOpen] = useState(false);
  const onSelected = e => {
    props.onSelected(e);
  };

  return (
    <Dropdown isOpen={isOpen} toggle={() => setIsOpen(!isOpen)}>
      <StyledDropdownToggle caret>
        {props.selectedLabel ? props.selectedLabel : `Select ${props.label}`}
      </StyledDropdownToggle>
      <StyledDropdownMenu>
        {Object.keys(props.optionMap).map(type => (
          <DropdownItem
            key={type}
            value={type}
            name={props.name}
            onClick={onSelected}
          >
            {props.optionMap[type]}
          </DropdownItem>
        ))}
      </StyledDropdownMenu>
    </Dropdown>
  );
};

export default CarPropDropdown;
