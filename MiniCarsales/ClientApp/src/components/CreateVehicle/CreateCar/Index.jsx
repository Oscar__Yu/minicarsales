import React, { useState, useEffect } from "react";
import { Alert } from "reactstrap";
import CreateCarForm from "./Form";
import CreatedList from "./CreatedList";

const CreateCar = () => {
  const [cars, setCars] = useState([]);
  const [bodyTypes, setBodyTypes] = useState({});
  const [makes, setMakes] = useState({});
  const [models, setModels] = useState({});
  const [engines, setEngines] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState("");

  const getBodyType = () =>
    fetch("api/car/bodytype")
      .then(response => response.json())
      .then(bodyTypes => {
        const bodyTypeMap = bodyTypes.reduce((map, type) => {
          map[type.id] = type.name;
          return map;
        }, {});
        setBodyTypes(bodyTypeMap);
      });
  const getMakes = () =>
    fetch("api/make")
      .then(response => response.json())
      .then(makes => {
        const makeMap = makes.reduce((map, type) => {
          map[type.id] = type.name;
          return map;
        }, {});
        setMakes(makeMap);
      });
  const getEngines = () =>
    fetch("api/engine")
      .then(response => response.json())
      .then(engines => {
        const engineMap = engines.reduce((map, type) => {
          map[type.id] = type.name;
          return map;
        }, {});
        setEngines(engineMap);
      });
  const getCars = () =>
    fetch("api/car")
      .then(response => response.json())
      .then(cars => {
        setCars(cars);
        setLoading(false);
      });
  const onCreated = car => {
    getCars();
    setError("");
  };
  const onCreateError = errMessage => {
    setError(errMessage);
  };
  const onModelLoaded = models => {
    const modelMap = models.reduce((map, type) => {
      map[type.id] = type.name;
      return map;
    }, {});
    setModels(modelMap);
  };

  useEffect(() => {
    getCars();
    getBodyType();
    getMakes();
    getEngines();
  }, []);

  return (
    <div>
      <h2>Create a car</h2>
      <CreateCarForm
        bodyTypes={bodyTypes}
        models={models}
        makes={makes}
        engines={engines}
        onCreated={onCreated}
        onCreateError={onCreateError}
        onModelLoaded={onModelLoaded}
      />

      {error ? <Alert color="danger">{error}</Alert> : undefined}

      <h2>Cars created</h2>
      {loading ? (
        <p>
          <em>Loading...</em>
        </p>
      ) : (
        <CreatedList cars={cars} />
      )}
    </div>
  );
};

export default CreateCar;
