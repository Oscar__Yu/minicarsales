import React from "react";
import styled from "styled-components";
import { Container, Navbar, NavbarBrand } from "reactstrap";
import { Link } from "react-router-dom";

const StyledNavbar = styled(Navbar)`
  box-shadow: 0 0.25rem 0.75rem rgba(0, 0, 0, 0.05);
`;

const Header = () => (
  <header>
    <StyledNavbar
      className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3"
      light
    >
      <Container>
        <NavbarBrand tag={Link} to="/">
          MiniCarsales Home
        </NavbarBrand>
      </Container>
    </StyledNavbar>
  </header>
);

export default Header;
