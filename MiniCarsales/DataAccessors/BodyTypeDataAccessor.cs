﻿using System.Collections.Generic;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Database;

namespace MiniCarsales.DataAccessors
{
    public interface IBodyTypeDataAccessor
    {
        BodyType Add(BodyType engine);
        IEnumerable<BodyType> GetAll();
    }

    public class BodyTypeDataAccessor : IBodyTypeDataAccessor
    {
        private readonly DataStore _dataStore;

        public BodyTypeDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public BodyType Add(BodyType bodyType)
        {
            var entityEntry = _dataStore.BodyTypes.Add(bodyType);
            _dataStore.SaveChanges();

            return entityEntry.Entity;
        }

        public IEnumerable<BodyType> GetAll()
        {
            return _dataStore.BodyTypes;
        }
    }
}
