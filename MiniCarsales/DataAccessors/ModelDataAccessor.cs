﻿using System.Collections.Generic;
using System.Linq;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;

namespace MiniCarsales.DataAccessors
{
    public interface IModelDataAccessor
    {
        Model Add(Model model);
        IEnumerable<Model> GetByMake(int makeId);
    }

    public class ModelDataAccessor : IModelDataAccessor
    {
        private readonly DataStore _dataStore;

        public ModelDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public Model Add(Model model)
        {
            var entityEntry = _dataStore.Models.Add(model);
            _dataStore.SaveChanges();

            return entityEntry.Entity;
        }

        public IEnumerable<Model> GetByMake(int makeId)
        {
            return _dataStore.Models.Where(m => m.MakeId == makeId);
        }
    }
}
