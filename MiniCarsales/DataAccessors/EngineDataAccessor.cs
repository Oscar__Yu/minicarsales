﻿using System.Collections.Generic;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;

namespace MiniCarsales.DataAccessors
{
    public interface IEngineDataAccessor
    {
        Engine Add(Engine engine);
        IEnumerable<Engine> GetAll();
    }

    public class EngineDataAccessor : IEngineDataAccessor
    {
        private readonly DataStore _dataStore;

        public EngineDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public Engine Add(Engine engine)
        {
            var entityEntry = _dataStore.Engines.Add(engine);
            _dataStore.SaveChanges();

            return entityEntry.Entity;
        }

        public IEnumerable<Engine> GetAll()
        {
            return _dataStore.Engines;
        }
    }
}
