﻿using System.Collections.Generic;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Database;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using MiniCarsales.Models.Contracts;

namespace MiniCarsales.DataAccessors
{
    public interface ICarDataAccessor
    {
        Car Add(Car car);
        IEnumerable<CarContractModel> GetAll();
    }

    public class CarDataAccessor : ICarDataAccessor
    {
        private readonly DataStore _dataStore;

        public CarDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public Car Add(Car car)
        {
            var entityEntry = _dataStore.Cars.Add(car);
            _dataStore.SaveChanges();
            return entityEntry.Entity;
        }

        public IEnumerable<CarContractModel> GetAll()
        {
            return _dataStore.Cars
                .Include(c => c.Model)
                .Include(c => c.Make)
                .Include(c => c.BodyType)
                .Include(c => c.Engine).Select(c => new CarContractModel {
                    Id = c.Id,
                    VehicleTypeId = c.VehicleTypeId,
                    Doors = c.Doors,
                    MakeId = c.MakeId,
                    MakeName = c.Make.Name,
                    ModelId = c.ModelId,
                    ModelName = c.Model.Name,
                    EngineId = c.EngineId,
                    EngineName = c.Engine.Name,
                    BodyTypeId = c.BodyTypeId,
                    BodyTypeName = c.BodyType.Name
                });
        }
    }
}
