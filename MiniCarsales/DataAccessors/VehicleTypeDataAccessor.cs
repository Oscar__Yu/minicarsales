﻿using System.Collections.Generic;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Database;

namespace MiniCarsales.DataAccessors
{
    public interface IVehicleTypeDataAccessor
    {
        VehicleType Add(VehicleType engine);
        IEnumerable<VehicleType> GetAll();
    }

    public class VehicleTypeDataAccessor : IVehicleTypeDataAccessor
    {
        private readonly DataStore _dataStore;

        public VehicleTypeDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public VehicleType Add(VehicleType vehicleType)
        {
            var entityEntry = _dataStore.VehicleTypes.Add(vehicleType);
            _dataStore.SaveChanges();

            return entityEntry.Entity;
        }

        public IEnumerable<VehicleType> GetAll()
        {
            return _dataStore.VehicleTypes;
        }
    }
}
