﻿using System.Collections.Generic;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;

namespace MiniCarsales.DataAccessors
{
    public interface IMakeDataAccessor
    {
        Make Add(Make make);
        IEnumerable<Make> GetAll();
    }

    public class MakeDataAccessor : IMakeDataAccessor
    {
        private readonly DataStore _dataStore;

        public MakeDataAccessor(DataStore dataStore)
        {
            _dataStore = dataStore;
        }

        public Make Add(Make make)
        {
            var entityEntry = _dataStore.Makes.Add(make);
            _dataStore.SaveChanges();

            return entityEntry.Entity;
        }

        public IEnumerable<Make> GetAll()
        {
            return _dataStore.Makes;
        }
    }
}
