using Xunit;
using MiniCarsales.Models.Vehicle;
using Microsoft.EntityFrameworkCore;
using MiniCarsales.Database;
using MiniCarsales.Test;

namespace MiniCarsales.DataAccessors.Test
{
    [Collection("Sequential Car Tests")]
    public class BodyTypeDataAccessorTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public BodyTypeDataAccessorTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldAddNewBodyType()
        {
            var bodyType = new BodyType() {
                Name = "Hatchback"
            };
            using (var context = new DataStore(_options))
            {
                var bodyTypeDataAccessor = new BodyTypeDataAccessor(context);
                var addedBodyType = bodyTypeDataAccessor.Add(bodyType);
                Assert.True(TestUtils.DeepCompare(bodyType, addedBodyType));
            }
        }
    }
}
