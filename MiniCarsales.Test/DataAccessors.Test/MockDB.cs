﻿using Microsoft.EntityFrameworkCore;
using MiniCarsales.Database;

namespace MiniCarsales.DataAccessors.Test
{
    public class MockDB
    {
        public MockDB()
        {
        }

        public static DbContextOptions<DataStore> GetMockDBBuilderOptions()
        {
            var builder = new DbContextOptionsBuilder<DataStore>();
            builder.UseInMemoryDatabase("InMemoryDataStore");
            return builder.Options;
        }
    }
}
