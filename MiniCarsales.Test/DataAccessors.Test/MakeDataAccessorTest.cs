using Xunit;
using MiniCarsales.Models.Common;
using Microsoft.EntityFrameworkCore;
using MiniCarsales.Database;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.DataAccessors;

namespace MiniCarsales.Test
{
    [Collection("Sequential Make Tests")]
    public class MakeDataAccessorTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public MakeDataAccessorTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldAddNewMake()
        {
            var make = new Make()
            {
                Name = "Make 1"
            };
            using (var context = new DataStore(_options))
            {
                var makeDataAccessor = new MakeDataAccessor(context);
                var addedMake = makeDataAccessor.Add(make);
                Assert.True(TestUtils.DeepCompare(make, addedMake));
            }
        }
    }
}
