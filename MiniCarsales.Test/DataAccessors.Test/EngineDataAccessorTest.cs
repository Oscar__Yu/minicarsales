using Xunit;
using MiniCarsales.Models.Common;
using Microsoft.EntityFrameworkCore;
using MiniCarsales.Database;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.DataAccessors;

namespace MiniCarsales.Test
{
    [Collection("Sequential Engine Tests")]
    public class EngineDataAccessorTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public EngineDataAccessorTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldAddNewEngine()
        {
            var engine = new Engine()
            {
                Name = "Engine"
            };
            using (var context = new DataStore(_options))
            {
                var engineDataAccessor = new EngineDataAccessor(context);
                var addedEngine = engineDataAccessor.Add(engine);
                Assert.True(TestUtils.DeepCompare(engine, addedEngine));
            }
        }
    }
}
