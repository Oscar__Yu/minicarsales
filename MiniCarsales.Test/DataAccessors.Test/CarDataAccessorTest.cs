using Xunit;
using MiniCarsales.Models.Vehicle;
using Microsoft.EntityFrameworkCore;
using MiniCarsales.Database;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.DataAccessors;

namespace MiniCarsales.Test
{
    [Collection("Sequential Car Tests")]
    public class CarDataAccessorTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public CarDataAccessorTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldAddNewCar()
        {
            var car = new Car()
            {
                BodyTypeId = 1,
                Doors = 2,
                EngineId = 1,
                MakeId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            using (var context = new DataStore(_options))
            {
                var carDataAccessor = new CarDataAccessor(context);
                var addedCar = carDataAccessor.Add(car);
                Assert.Equal(car.BodyTypeId, addedCar.BodyTypeId);
                Assert.Equal(car.Doors, addedCar.Doors);
                Assert.Equal(car.EngineId, addedCar.EngineId);
                Assert.Equal(car.ModelId, addedCar.ModelId);
                Assert.Equal(car.MakeId, addedCar.MakeId);
                Assert.Equal(car.VehicleTypeId, addedCar.VehicleTypeId);
            }
        }
    }
}
