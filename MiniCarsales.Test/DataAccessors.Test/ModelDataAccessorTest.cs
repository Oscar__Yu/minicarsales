using Xunit;
using Microsoft.EntityFrameworkCore;
using MiniCarsales.Models.Common;
using MiniCarsales.Database;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.DataAccessors;

namespace MiniCarsales.Test
{
    [Collection("Sequential Model Tests")]
    public class ModelDataAccessorTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public ModelDataAccessorTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldAddNewModel()
        {
            var model = new Model()
            {
                Name = "Model 1"
            };
            using (var context = new DataStore(_options))
            {
                var modelDataAccessor = new ModelDataAccessor(context);
                var addedModel = modelDataAccessor.Add(model);
                Assert.True(TestUtils.DeepCompare(model, addedModel));
            }
        }
    }
}
