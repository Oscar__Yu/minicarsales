using Xunit;
using MiniCarsales.Models.Contracts;
using FluentValidation.TestHelper;

namespace MiniCarsales.Models.Validators.Test
{
    public class CarValidatorTest
    {
        [Fact]
        public void ValidCarMustPassValidator()
        {
            var car = new CarContractModel() {
                BodyTypeId = 1,
                Doors = 2,
                EngineId = 1,
                MakeId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            Assert.True(validator.Validate(car).IsValid);
        }

        [Fact]
        public void DoorsMustNotBeSmallerThanTwo()
        {
            var car = new CarContractModel()
            {
                BodyTypeId = 1,
                Doors = 1,
                EngineId = 1,
                MakeId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            validator.ShouldHaveValidationErrorFor(c => c.Doors, car);
        }

        [Fact]
        public void BodyTypeMustNotBeEmpty()
        {
            var car = new CarContractModel()
            {
                Doors = 2,
                EngineId = 1,
                MakeId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            validator.ShouldHaveValidationErrorFor(c => c.BodyTypeId, car);
        }

        [Fact]
        public void EngineMustNotBeEmpty()
        {
            var car = new CarContractModel()
            {
                BodyTypeId = 1,
                Doors = 2,
                MakeId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            validator.ShouldHaveValidationErrorFor(c => c.EngineId, car);
        }

        [Fact]
        public void MakeMustNotBeEmpty()
        {
            var car = new CarContractModel()
            {
                BodyTypeId = 1,
                Doors = 2,
                EngineId = 1,
                ModelId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            validator.ShouldHaveValidationErrorFor(c => c.MakeId, car);
        }

        [Fact]
        public void ModelMustNotBeEmpty()
        {
            var car = new CarContractModel()
            {
                BodyTypeId = 1,
                Doors = 2,
                EngineId = 1,
                MakeId = 1,
                VehicleTypeId = 1
            };
            var validator = new CarValidator();
            validator.ShouldHaveValidationErrorFor(c => c.ModelId, car);
        }
    }
}
