using Xunit;
using MiniCarsales.Models.Common;
using FluentValidation.TestHelper;

namespace MiniCarsales.Models.Validators.Test
{
    public class CommonPropValidatorTest
    {
        [Fact]
        public void ValidMakeMustPassValidator()
        {
            var make = new Make() {
                Name = "Make 1"
            };
            var validator = new MakeValidator();
            Assert.True(validator.Validate(make).IsValid);
        }

        [Fact]
        public void MakeMustHaveAName()
        {
            var make = new Make()
            {
                Name = ""
            };
            var validator = new MakeValidator();
            validator.ShouldHaveValidationErrorFor(m => m.Name, make);
        }

        [Fact]
        public void ValidModelMustPassValidator()
        {
            var model = new Model()
            {
                Name = "Model 1"
            };
            var validator = new ModelValidator();
            Assert.True(validator.Validate(model).IsValid);
        }

        [Fact]
        public void ModelMustHaveAName()
        {
            var model = new Model()
            {
                Name = ""
            };
            var validator = new ModelValidator();
            validator.ShouldHaveValidationErrorFor(m => m.Name, model);
        }

        [Fact]
        public void ValidEngineMustPassValidator()
        {
            var engine = new Engine()
            {
                Name = "Engine 1"
            };
            var validator = new EngineValidator();
            Assert.True(validator.Validate(engine).IsValid);
        }

        [Fact]
        public void EngineMustHaveAName()
        {
            var engine = new Engine()
            {
                Name = ""
            };
            var validator = new EngineValidator();
            validator.ShouldHaveValidationErrorFor(e => e.Name, engine);
        }
    }
}
