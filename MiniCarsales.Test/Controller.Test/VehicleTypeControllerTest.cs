﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using MiniCarsales.Controllers;
using MiniCarsales.DataAccessors;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.Database;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Services;
using Xunit;

namespace MiniCarsales.Test
{
    [Collection("Sequential Vehicle Type Tests")]
    public class VehicleTypeControllerTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public VehicleTypeControllerTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldCreateCars()
        {
            using (var context = new DataStore(_options))
            using (var controller = new VehicleTypeController(
                new VehicleTypeServices(new VehicleTypeDataAccessor(context)),
                new NullLogger<VehicleTypeController>()
                ))
            {
                // Create vehicle
                var query = new VehicleType()
                {
                    Name = "Car"
                };
                CreatedResult vehicleCreated = controller.Post(query).Result as CreatedResult;
                Assert.True(TestUtils.DeepCompare(vehicleCreated.Value, query));

                // Retrieve vehicle
                var vehicles = (controller.Get().Result as OkObjectResult).Value;
                var vehicleList = new List<VehicleType>(vehicles as IEnumerable<VehicleType>);
                Assert.True(TestUtils.DeepCompare(vehicleList[0], query));
            }
        }
    }
}
