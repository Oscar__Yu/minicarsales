﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using MiniCarsales.Controllers;
using MiniCarsales.DataAccessors;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;
using MiniCarsales.Services;
using Xunit;

namespace MiniCarsales.Test
{
    [Collection("Sequential Make Tests")]
    public class MakeControllerTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public MakeControllerTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldCreateCars()
        {
            using (var context = new DataStore(_options))
            using (var controller = new MakeController(
                new CommonPropertyServices(
                    new MakeDataAccessor(context),
                    new ModelDataAccessor(context),
                    new EngineDataAccessor(context)),
                new NullLogger<MakeController>()
                ))
            {
                // Create make
                var query = new Make()
                {
                    Name = "Make 1"
                };
                CreatedResult makeCreated = controller.Post(query).Result as CreatedResult;
                Assert.True(TestUtils.DeepCompare(makeCreated.Value, query));

                // Retrieve make
                var makes = (controller.Get().Result as OkObjectResult).Value;
                var makeList = new List<Make>(makes as IEnumerable<Make>);
                Assert.True(TestUtils.DeepCompare(makeList[0], query));
            }
        }
    }
}
