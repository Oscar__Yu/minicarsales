﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using MiniCarsales.Controllers;
using MiniCarsales.DataAccessors;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.Database;
using MiniCarsales.Models.Common;
using MiniCarsales.Services;
using Xunit;

namespace MiniCarsales.Test
{
    [Collection("Sequential Model Tests")]
    public class ModelControllerTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public ModelControllerTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldCreateCars()
        {
            using (var context = new DataStore(_options))
            using (var controller = new ModelController(
                new CommonPropertyServices(
                    new MakeDataAccessor(context),
                    new ModelDataAccessor(context),
                    new EngineDataAccessor(context)),
                new NullLogger<ModelController>()
                ))
            {
                // Create model
                var query = new Model()
                {
                    Name = "Model 1",
                    MakeId = 1
                };
                CreatedResult modelCreated = controller.Post(query).Result as CreatedResult;
                Assert.True(TestUtils.DeepCompare(modelCreated.Value, query));

                // Retrieve model
                var models = (controller.Get(1).Result as OkObjectResult).Value;
                var modelList = new List<Model>(models as IEnumerable<Model>);
                Assert.True(TestUtils.DeepCompare(modelList[0], query));
            }
        }
    }
}
