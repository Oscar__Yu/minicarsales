﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging.Abstractions;
using MiniCarsales.Controllers;
using MiniCarsales.DataAccessors;
using MiniCarsales.DataAccessors.Test;
using MiniCarsales.Database;
using MiniCarsales.Models.Contracts;
using MiniCarsales.Models.Vehicle;
using MiniCarsales.Services;
using Xunit;

namespace MiniCarsales.Test
{
    [Collection("Sequential Car Tests")]
    public class CarControllerTest
    {
        private readonly DbContextOptions<DataStore> _options;

        public CarControllerTest()
        {
            _options = MockDB.GetMockDBBuilderOptions();
        }

        [Fact]
        public void ShouldCreateCars()
        {
            using (var context = new DataStore(_options))
            using (var controller = new CarController(
                new CarServices(new CarDataAccessor(context), new BodyTypeDataAccessor(context)),
                new NullLogger<CarController>()
                ))
            {
                var query = new CarContractModel()
                {
                    BodyTypeId = 1,
                    Doors = 2,
                    EngineId = 1,
                    MakeId = 1,
                    ModelId = 1,
                    VehicleTypeId = 1
                };
                CreatedResult carCreated = controller.Post(query).Result as CreatedResult;
                Assert.True(TestUtils.DeepCompare(carCreated.Value, query));
            }
        }

        [Fact]
        public void ShouldGetBodyTypes()
        {
            using (var context = new DataStore(_options))
            {
                // Arrange
                var carDataAccessor = new CarDataAccessor(context);
                var bodyTypeDataAccessor = new BodyTypeDataAccessor(context);
                using (var controller = new CarController(
                    new CarServices(carDataAccessor, bodyTypeDataAccessor),
                    new NullLogger<CarController>()
                    ))
                {
                    var bodyTypes = (controller.GetBodyType().Result as OkObjectResult).Value;
                    var bodyTypesList = new List<BodyType>(bodyTypes as IEnumerable<BodyType>);
                    var originalCount = bodyTypesList.Count;

                    var query = new BodyType()
                    {
                        Name = "Hatchback"
                    };
                    bodyTypeDataAccessor.Add(query);

                    bodyTypes = (controller.GetBodyType().Result as OkObjectResult).Value;
                    bodyTypesList = new List<BodyType>(bodyTypes as IEnumerable<BodyType>);
                    Assert.True(bodyTypesList.Count > originalCount);
                }
            }
        }
    }
}
