﻿using Newtonsoft.Json;

namespace MiniCarsales.Test
{
    public static class TestUtils
    {
        public static bool DeepCompare(object obj1, object obj2)
        {
            var obj1Str = JsonConvert.SerializeObject(obj1);
            var obj2Str = JsonConvert.SerializeObject(obj2);
            return obj1Str == obj2Str;
        }
    }
}
