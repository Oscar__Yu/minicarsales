# Mini Carsales

A web app allowing users to create vehicles (catered for cars only at this moment but will support other vehicles e.g. Boats in the future).

- Server written in ASP.NET Core and C#
- Client written in JS and React

## Project file structure

    MiniCarsales/               ----- Source code root dir
    MiniCarsales/Database       ----- In-memory data store using EF, and seed data
    MiniCarsales/Models         ----- Database table definitions, Data types and interfaces defined for APIs, fluent validation
    MiniCarsales/Controllers    ----- API request handlers
    MiniCarsales/DataAccessors  ----- Database accessor, accessing and retrieving data from db
    MiniCarsales/Services       ----- Business logic service layer
    MiniCarsales/clientApp      ----- Frontend JS and React source code, with UI component unit tests using Jest and react-test-renderer
    MiniCarsales.Test/          ----- ASP.NET server app unit test

## Build and Run (Developed on MacOS)

1. Install Nodejs with NPM package manager from [here](https://nodejs.org/en/download/).

2. Install Visual Studio 2019 from [here](https://visualstudio.microsoft.com/vs/) with .NET Core, and open project solution

3. Restore dependencies:

    For each the 2 projects (`MiniCarsales` and `MiniCarsales.Test`): right click `dependencies` folder in source tree in VS 2019, and click `Restore`

4. Build projects:

    right click `MiniCarsales` solution at the root in source tree in VS 2019, and click `Build MiniCarsales`

5. Run `MiniCarsales` project

## Test

### Build and run ASP.NET unit tests

1. Restore dependencies as described above
2. Run Unit tests fron VS 2019 (MacOS: go to `View` -> `Test` and Open `Unit Tests` window and click `Run All`)

### Build and run Jest unit tests for UI components

1. CD to `ClientApp` folder, and run `npm install`
2. Stay in the same dir, run `npm test`
